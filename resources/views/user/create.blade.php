@extends('layouts.app')

@section('title')
	Nieuwe gebruiker toevoegen
@endsection

@section('tools')
<li role="navigation">
	<a onClick="window.history.back()">
		<i class="fa fa-arrow-left"></i>&nbspTerug
	</a>
</li>
@endsection

@section('content')
{!! Form::open(['route' => ['user.store'], 'method' => 'post', 'class' => 'form-horizontal', 'data-parsley-validate' => '']) !!}
<div class="form-group">
	<div class="col-sm-7">
		{!! Form::label('name', 'Naam', ['class' => 'control-label']) !!}
		{!! Form::text('name', null, [
				'class' => 'form-control',
				'placeholder' => 'Vul een naam in',
				'required' => '',
				'data-parsley-required-message' => 'Naam is verplicht',
				'data-parsley-trigger' => 'change focusout'
		]) !!}
	</div>

	<div class="col-sm-7">
		{!! Form::label('email', 'Email', ['class' => 'control-label']) !!}
		{!! Form::email('email', null, [
				'class' => 'form-control',
				'placeholder' => 'Vul een e-mailadres in',
				'required' => '',
				'data-parsley-required-message' => 'E-mail is verplicht',
				'data-parsley-trigger' => 'change focusout'
		]) !!}
	</div>

	<div class="col-sm-7">
		{!! Form::label('password', 'Wachtwoord', ['class' => 'control-label']) !!}
		{!! Form::password('password', [
				'class'                         => 'form-control',
				'placeholder'                   => 'Vul een wachtwoord in',
				'required',
				'id'                            => 'inputPassword',
				'data-parsley-required-message' => 'Een wachtwoord is verplicht',
				'data-parsley-trigger'          => 'change focusout',
				'data-parsley-minlength'        => '6',
				'data-parsley-maxlength'        => '20'
		]) !!}

		{!! Form::password('password_confirmation', [
				'class'                         => 'form-control',
				'placeholder'                   => 'Bevestig het wachtwoord',
				'data-parsley-required',
				'id'                            => 'inputPasswordConfirm',
				'data-parsley-required-message' => 'Bevestig het wachtwoord',
				'data-parsley-trigger'          => 'change focusout',
				'data-parsley-equalto'          => '#inputPassword',
				'data-parsley-equalto-message'  => 'Komt niet overeen met het wachtwoord',
		]) !!}
	</div>

	<div class="col-sm-7">
		{!! Form::label('role_id', 'Rol', ['class' => 'control-label']) !!}
		{!! Form::select('role_id', $roles, null, [
				'class' => 'form-control',
				'placeholder' => 'Maak een keuze uit de lijst',
				'required' => '',
				'data-parsley-trigger' => 'change focusout'
		]) !!}
	</div>
	<div class="col-sm-7">
		{!! Form::label('location_id', 'Locatie', ['class' => 'control-label']) !!}
		{!! Form::select('location_id', $locations, null, [
				'class' => 'form-control',
				'placeholder' => 'Maak een keuze uit de lijst'
		]) !!}
	</div>
</div>

<div class="form-group">
	<div class="col-sm-12">
		<button type="submit" class="btn btn-primary">
			Opslaan
		</button>
	</div>
</div>
{!! Form::close() !!}
@endsection

@section('scripts')
<script type="text/javascript" src="{{ URL::asset('js/jquery-3.1.1.slim.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/parsley.min.js') }}"></script>
@endsection
