# BIEPOnline
This project is a prototype for a library support system for a specific primary school. It serves as the context of an assignemt project for ICT students to study software engineering collaboration processes and skills.

# PoC Testing
This project has been used for an assignment to create a new validation system. This is only implemented in ONE page, which is the 'create a new user' page.

# Contributors to PoC Testing
* Mike Vissers & Nicole Yang

# Contributors to BIEPOnline
* Daan de Waard
* Allison van de Kreke 00072161 & Steven Nassy 00072032
* Tim Banh 00073813 & Delano Brasz 00073026
* Giovanni van Pamelen 00073248 & Rob Vader 00074599
* Joris Bakx 00073350 & Sven de Vries 00065383
* Matthijs Bijkerk 00072658 & Giorgio Joziasse 00073436
* Jasper de Pooter 0072671 & Thomas Roth 00072994
* Bas van Dam 00072911 & Joep Oomens 00073339
* Shawn Witte 00072673 & Jano van der Dussen 00072877
* Nicole Yang 00073839 & Xia de Visser 00073771
* Ashley de Looff 00073606 & Milad Rahmani 00074069
* Bart Franse 0074230 & Geffrey de Winter 00074228
* Jordy Lynch 00073712 & Robin Broeks 00073054
* Tim Abspoel 00072981 & Sytze Kamermans 00074216
